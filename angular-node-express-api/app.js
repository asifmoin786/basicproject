var express = require('express');
var path = require('path');
// var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const port = 3000;
var users = require('./routes/users');
const allowedExt = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',

];
var app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser())

// app.use('/api/generate_uid', generate_uid);

// app.use('/api/users', users);

app.get('/api/hello', (req, res) => res.send('Hello World!'));
console.log( __dirname + "/dist/basicproject/index.html");

// app.get('*', (req, res) =>  res.sendFile(path.resolve('/dist/basicproject/index.html'), { root : __dirname}));

app.get('*', (req, res) => {
  if (allowedExt.filter(ext => req.url.indexOf(ext) > 0).length > 0) {
    res.sendFile(path.resolve(`dist/basicproject/${req.url}`));
  } else {
    res.sendFile(path.resolve('dist/basicproject/index.html'));
  }
});

// module.exports = app;
app.listen(port, () => console.log(`Example app listening on port ${port}!`))


